# MS-INFOCAD

[[_TOC_]]

### Objetivo

Este projeto refere-se a uma POC de demonstração de arquitetura do projeto "Logística Baseada em MicrosServiços" dos alunos Felipe Almeida da Graça e Pedro Henrique Ribeiro Lopes, projeto equivalente a requisito de encerramento do curso de pós-graduação _lato sensu_ em Arquitetura de Software Distribuído da PUC-MG, em modelo EAD.

Deste modo, este projeto em específico equivale ao microsserviço do "Módulo de Informações Cadastrais", o qual implementa, de forma simplificada, os casos de uso de cadastro de clientes, fornecedores e mercadorias.


### Execução em ambiente de demonstração

#### Passos gerais de preparo (necessários a ambos os cenários):
- Git instalado (Para instruções de instalação: [clique aqui](https://git-scm.com/downloads));
- Java JDK instalado (Recomendamos o OpenJDK por ser opensource. Para instruções de instalação: [clique aqui](https://openjdk.org/install/));
- Baixe este projeto a um diretório em seu ambiente local (Para instruções de como clonar um projeto com git: [clique aqui](https://www.youtube.com/watch?v=WEPB5pDSEIg));
- ms-authserver em execução (para tal, veja respectivo [readme.me](https://gitlab.com/pucmg/poc-estoque/ms-authserver/-/blob/main/README.md));

#### Cenário 1: Por IDE em ambiente local (recomendado para quem deseja avaliar o código)

##### Prepare o seu ambiente:
- IDE habilitada para Java com suporte a Spring Boot (Recomendamos a STS Eclipse por ser customizado pela spring. Para instruçõe sde instalação: [clique aqui](https://spring.io/tools));
- Instale o Lombok na sua IDE (Para instruçõe de instalação: [clique aqui](https://projectlombok.org/download));

##### Agora vamos à execução (passos considerando o STS):
1. Abra a STS através de seu executável no diretório de instalação;
2. Em "Arquivo" (File) e clique em "Abrir projetos do seu sistema de arquivos" (Open Projects From File System);
3. Selecione o diretório onde está a aplicação baixada, através do botão "Directory";
4. Selecione a aplicação na lista de aplicações apresentadas após a leitura do diretório;
5. Clique com o botão direito sobre o projeto `ms-infocad`, na aba "Project Explorer", selecione a opção "Execute como" (Run As) e então "Aplicação Spring Boot" (Spring Boot App);

#### Cenário 2: Por container

##### Prepare o seu ambiente:
- Instale o docker (Para instruções de instalação [clique aqui](https://www.docker.com/get-started/))

##### Agora vamos à execução:
1. Para compilar a imagem: `docker build . -t ms-infocad:v1`
2. Uma vez compilada a imagem, para executar a imagem: `docker run ms-infocad:v1 -p 8080:8080`

#### Passos pós execução:
- Uma vez executado, a aplicação estará em execução, acessível localmente, via browser de sua preferência, por: `http://localhost:8080/`
- Caso deseje visualizar os serviços, e até mesmo simular seu uso, acesse: `http://localhost:8080/swagger-ui.html`
   - Para geração de token de sessão (uma vez que todas as requisições deste módulo dependem de uma sessão válida em execução), consulte o endpoint token do serviço ms-authserver.