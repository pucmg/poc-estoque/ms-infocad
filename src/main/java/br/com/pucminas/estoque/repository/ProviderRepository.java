package br.com.pucminas.estoque.repository;

import br.com.pucminas.estoque.model.entities.ProviderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProviderRepository extends JpaRepository<ProviderEntity, Long>{
	
	List<ProviderEntity> findAll();

	@Override
	Optional<ProviderEntity> findById(Long id);

	Optional<ProviderEntity> findByLegalName(String legalname);
	
	Optional<ProviderEntity> findByPublicName(String publicName);

	Optional<ProviderEntity> findByDocument(String document);
	
	@Query(value = "SELECT * FROM PROVIDER WHERE SPECIALIZATION = ?1 ", nativeQuery = true)
	Optional<List<ProviderEntity>> findBySpecialization(String specialization);
	
	@Query(value = "SELECT * FROM PROVIDER WHERE LOCATION = ?1", nativeQuery = true)
	Optional<List<ProviderEntity>> findByLocation(String location);

	@Query(value = "SELECT * FROM PROVIDER WHERE SPECIALIZATION = ?1 AND LOCATION = ?2", nativeQuery = true)
	Optional<List<ProviderEntity>> findBySpecializationAndLocation(String specialization, String location);
}
