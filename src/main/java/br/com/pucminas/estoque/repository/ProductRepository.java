package br.com.pucminas.estoque.repository;

import br.com.pucminas.estoque.model.entities.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Long>{

	List<ProductEntity> findAll();

	@Override
	Optional<ProductEntity> findById(Long id);

	Optional<ProductEntity> findBySku(Long sku);
	
	Optional<ProductEntity> findByName(String name);

}
