package br.com.pucminas.estoque.repository;

import br.com.pucminas.estoque.model.entities.ClientEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ClientRepository extends JpaRepository<ClientEntity, Long>{

	List<ClientEntity> findAll();

	@Override
	Optional<ClientEntity> findById(Long id);

	Optional<ClientEntity> findByName(String name);
	
	Optional<ClientEntity> findByDocument(String document);


}

