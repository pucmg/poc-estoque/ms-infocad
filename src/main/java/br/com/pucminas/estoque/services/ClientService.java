package br.com.pucminas.estoque.services;

import br.com.pucminas.estoque.model.entities.ClientEntity;
import br.com.pucminas.estoque.model.request.ClientRequestDTO;
import br.com.pucminas.estoque.model.response.ClientResponseDTO;
import br.com.pucminas.estoque.repository.ClientRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@Service
public class ClientService {

    private final ClientRepository clientRepo;
    private Logger logger = LoggerFactory.getLogger(ClientService.class);

    @Autowired
    public ClientService(ClientRepository userRepo){
        this.clientRepo = userRepo;
    }

    public ResponseEntity<List<ClientResponseDTO>> findAll() {
        var clientsUntity = clientRepo.findAll();
        List<ClientResponseDTO> clientsResponseDTO = new ArrayList<ClientResponseDTO>();

        if(clientsUntity.isEmpty()){
            logger.info("[MS-INFOCAD ] - ::GET ALL CLIENTS:: - No Content - response: " );
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }else {
            clientsUntity.forEach(client -> {
                clientsResponseDTO.add(client.toResponseDTO());
            });
        }
        var response = ResponseEntity.ok().body(clientsResponseDTO);

        logger.info("[MS-INFOCAD ] - ::GET ALL CLIENTS:: - Successful - response: " + response);
        return ResponseEntity.ok().body(clientsResponseDTO);
    }

    public ResponseEntity<ClientResponseDTO> findById(Long id) {
        var clientEntity = clientRepo.findById(id);

        if (clientEntity.isPresent()) {
            var response = clientEntity.get().toResponseDTO();

            logger.info("[MS-INFOCAD ] - ::GET CLIENT BY ID:: - Successful - id: " + id + "response: " + response);
            return ResponseEntity.ok().body(response);
        }else{
            logger.info("[MS-INFOCAD ] - ::GET CLIENT BY ID:: - Not found - response: ");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    public ResponseEntity<ClientResponseDTO> save(ClientRequestDTO clientRequestDTO, UriComponentsBuilder uriBuilder) {

        ClientEntity clientEntity = clientRequestDTO.toEntity();
        try {
            var clientEntitySaved = clientRepo.save(clientEntity);
            URI uri = uriBuilder.path("api/v1/client/{id}").buildAndExpand(clientEntity.getId()).toUri();
            var response = clientEntitySaved.toResponseDTO();

            logger.info("[MS-INFOCAD ] - ::ADD NEW CLIENT:: - Successful - response: " + response + " uri: " + uri);
            return ResponseEntity.created(uri).body(response);
        }catch (Exception e){
            logger.error("[MS-INFOCAD ] - ::ADD NEW CLIENT:: - Fail - exception: " + e.getLocalizedMessage());
        }

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    public ResponseEntity<?> deleteById(Long id) {
        var clientEntity = clientRepo.findById(id);

        if(clientEntity.isPresent()){
            clientRepo.deleteById(id);
            logger.info("[MS-INFOCAD ] - ::DELETE CLIENT:: - Successful - id: " + id);
            return ResponseEntity.ok().build();
        }

        logger.info("[MS-INFOCAD ] - ::DELETE CLIENT:: - No Found - id: " + id);

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();

    }

    public ResponseEntity<?> updateById(Long id, ClientRequestDTO updatedClientRequestDTO) {
        var clientEntity = clientRepo.findById(id);

        if(clientEntity.isPresent()){
            clientEntity.get().update(updatedClientRequestDTO.toEntity());

            logger.info("[MS-INFOCAD ] - ::UPDATE CLIENT:: - Successful - id: " + id);
            return ResponseEntity.ok().build();
        }

        logger.info("[MS-INFOCAD ] - ::UPDATE CLIENT:: - No Found - id: " + id);

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();

    }
    
    public ResponseEntity<ClientResponseDTO> findByName(String name){
        var clientEntity = clientRepo.findByName(name);

        if (clientEntity.isPresent()) {
            var response = clientEntity.get().toResponseDTO();

            logger.info("[MS-INFOCAD ] - ::GET CLIENT BY NAME:: - Successful - NAME: " + name + ", response: " + response);
            return ResponseEntity.ok().body(response);
        }else{
            logger.info("[MS-INFOCAD ] - ::GET CLIENT BY NAME:: - Not found - response: ");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    public ResponseEntity<ClientResponseDTO> findByDocument(String document){
        var clientEntity = clientRepo.findByDocument(document);

        if (clientEntity.isPresent()) {
            var response = clientEntity.get().toResponseDTO();

            logger.info("[MS-INFOCAD ] - ::GET CLIENT BY DOCUMENT:: - Successful - DOCUMENT: " + document + ", response: " + response);
            return ResponseEntity.ok().body(response);
        }else{
            logger.info("[MS-INFOCAD ] - ::GET CLIENT BY DOCUMENT:: - Not found - response: ");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
    
}
