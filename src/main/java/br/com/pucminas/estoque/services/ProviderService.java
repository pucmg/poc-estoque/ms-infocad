package br.com.pucminas.estoque.services;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import br.com.pucminas.estoque.model.entities.ProviderEntity;
import br.com.pucminas.estoque.model.request.ProviderRequestDTO;
import br.com.pucminas.estoque.model.response.ProviderResponseDTO;
import br.com.pucminas.estoque.repository.ProviderRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class ProviderService {

    private final ProviderRepository providerRepo;
    private Logger logger = LoggerFactory.getLogger(ProviderService.class);

    @Autowired
    public ProviderService(ProviderRepository providerRepo){
        this.providerRepo = providerRepo;
    }

    public ResponseEntity<List<ProviderResponseDTO>> findAll() {
        var providersUntity = providerRepo.findAll();
        List<ProviderResponseDTO> providersResponseDTO = new ArrayList<ProviderResponseDTO>();

        if(providersUntity.isEmpty()){
            logger.info("[MS-INFOCAD ] - ::GET ALL PROVIDERS:: - No Content - response: " );
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }else {
            providersUntity.forEach(provider -> {
                providersResponseDTO.add(provider.toResponseDTO());
            });
        }
        var response = ResponseEntity.ok().body(providersResponseDTO);

        logger.info("[MS-INFOCAD ] - ::GET ALL PROVIDERS:: - Successful - response: " + response);
        return ResponseEntity.ok().body(providersResponseDTO);
    }

    public ResponseEntity<ProviderResponseDTO> findById(Long id) {
        var providerEntity = providerRepo.findById(id);

        if (providerEntity.isPresent()) {
            var response = providerEntity.get().toResponseDTO();

            logger.info("[MS-INFOCAD ] - ::GET PROVIDER BY ID:: - Successful - id: " + id + "response: " + response);
            return ResponseEntity.ok().body(response);
        }else{
            logger.info("[MS-INFOCAD ] - ::GET PROVIDER BY ID:: - Not found - response: ");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
    
    public ResponseEntity<ProviderResponseDTO> findByLegalName(String legalName) {
        var providerEntity = providerRepo.findByLegalName(legalName);

        if (providerEntity.isPresent()) {
            var response = providerEntity.get().toResponseDTO();

            logger.info("[MS-INFOCAD ] - ::GET PROVIDER BY LEGALNAME:: - Successful - legalName: " + legalName + "response: " + response);
            return ResponseEntity.ok().body(response);
        }else{
            logger.info("[MS-INFOCAD ] - ::GET PROVIDER BY LEGALNAME:: - Not found - response: ");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
    
    public ResponseEntity<ProviderResponseDTO> findByPublicName(String publicName) {
        var providerEntity = providerRepo.findByPublicName(publicName);

        if (providerEntity.isPresent()) {
            var response = providerEntity.get().toResponseDTO();

            logger.info("[MS-INFOCAD ] - ::GET PROVIDER BY PUBLICNAME:: - Successful - publicName: " + publicName + "response: " + response);
            return ResponseEntity.ok().body(response);
        }else{
            logger.info("[MS-INFOCAD ] - ::GET PROVIDER BY PUBLICNAME:: - Not found - response: ");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
    
    public ResponseEntity<ProviderResponseDTO> findByDocument(String document) {
        var providerEntity = providerRepo.findByDocument(document);

        if (providerEntity.isPresent()) {
            var response = providerEntity.get().toResponseDTO();

            logger.info("[MS-INFOCAD ] - ::GET PROVIDER BY DOCUMENT:: - Successful - document: " + document + "response: " + response);
            return ResponseEntity.ok().body(response);
        }else{
            logger.info("[MS-INFOCAD ] - ::GET PROVIDER BY DOCUMENT:: - Not found - response: ");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
    
    public ResponseEntity<List<ProviderResponseDTO>> findBySpecialization(String specialization) {
        var providersEntity = providerRepo.findBySpecialization(specialization);
        List<ProviderResponseDTO> providersList = new ArrayList<ProviderResponseDTO>();

        if (providersEntity.isPresent()) {
            providersEntity.get().forEach(providerEntity -> {
                if(providerEntity.getSpecialization().equals(specialization)){
                    providersList.add(providerEntity.toResponseDTO());
                }
            });

            logger.info("[MS-INFOCAD ] - ::GET PROVIDERS BY SPECIALIZATION:: - Successful");
            return ResponseEntity.ok().body(providersList);
        }else{
            logger.info("[MS-INFOCAD ] - ::GET PROVIDERS BY SPECIALIZATION:: - Not found - response: ");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
        
    public ResponseEntity<List<ProviderResponseDTO>> findByLocation(String location) {
    	var providersEntity = providerRepo.findByLocation(location);
        List<ProviderResponseDTO> providersList = new ArrayList<ProviderResponseDTO>();

        if (providersEntity.isPresent()) {
            providersEntity.get().forEach(providerEntity -> {
                if(providerEntity.getLocation().equals(location)){
                    providersList.add(providerEntity.toResponseDTO());
                }
            });

            logger.info("[MS-INFOCAD ] - ::GET PROVIDERS BY LOCATION:: - Successful");
            return ResponseEntity.ok().body(providersList);
        }else{
            logger.info("[MS-INFOCAD ] - ::GET PROVIDERS BY LOCATION:: - Not found - response: ");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<List<ProviderResponseDTO>> findBySpecializationAndLocation(String specialization, String location){

        var providersEntity = providerRepo.findBySpecializationAndLocation(specialization, location);
        List<ProviderResponseDTO> providersList = new ArrayList<ProviderResponseDTO>();

        if (providersEntity.isPresent()) {
            providersEntity.get().forEach(providerEntity -> {
                if(providerEntity.getSpecialization().equals(specialization)){
                    providersList.add(providerEntity.toResponseDTO());
                }
            });

            logger.info("[MS-INFOCAD ] - ::GET PROVIDERS BY SPECIALIZATION AND LOCATION:: - Successful");
            return ResponseEntity.ok().body(providersList);
        }else{
            logger.info("[MS-INFOCAD ] - ::GET PROVIDERS BY SPECIALIZATION AND LOCATION:: - Not found - response: ");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    public ResponseEntity<ProviderResponseDTO> save(ProviderRequestDTO providerRequestDTO, UriComponentsBuilder uriBuilder) {

        ProviderEntity providerEntity = providerRequestDTO.toEntity();
        try {
            var providerEntitySaved = providerRepo.save(providerEntity);
            URI uri = uriBuilder.path("api/v1/provider/{id}").buildAndExpand(providerEntity.getId()).toUri();
            var response = providerEntitySaved.toResponseDTO();

            logger.info("[MS-INFOCAD ] - ::ADD NEW PROVIDER:: - Successful - response: " + response + " uri: " + uri);
            return ResponseEntity.created(uri).body(response);
        }catch (Exception e){
            logger.error("[MS-INFOCAD ] - ::ADD NEW PROVIDER:: - Fail - exception: " + e.getLocalizedMessage());
        }

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    public ResponseEntity<?> deleteById(Long id) {
        var providerEntity = providerRepo.findById(id);

        if(providerEntity.isPresent()){
            providerRepo.deleteById(id);
            logger.info("[MS-INFOCAD ] - ::DELETE PROVIDER:: - Successful - id: " + id);
            return ResponseEntity.ok().build();
        }

        logger.info("[MS-INFOCAD ] - ::DELETE PROVIDER:: - No Found - id: " + id);

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();

    }

    public ResponseEntity<?> updateById(Long id, ProviderRequestDTO updatedProviderRequestDTO) {
        var providerEntity = providerRepo.findById(id);

        if(providerEntity.isPresent()){
            providerEntity.get().update(updatedProviderRequestDTO.toEntity());

            logger.info("[MS-INFOCAD ] - ::UPDATE PROVIDER:: - Successful - id: " + id);
            return ResponseEntity.ok().build();
        }

        logger.info("[MS-INFOCAD ] - ::UPDATE PROVIDER:: - No Found - id: " + id);

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();

    }
    
}
