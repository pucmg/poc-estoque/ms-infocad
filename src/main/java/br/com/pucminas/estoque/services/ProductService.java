package br.com.pucminas.estoque.services;

import br.com.pucminas.estoque.model.entities.ProductEntity;
import br.com.pucminas.estoque.model.request.ProductRequestDTO;
import br.com.pucminas.estoque.model.response.ProductResponseDTO;
import br.com.pucminas.estoque.repository.ProductRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {

    private final ProductRepository productRepo;
    private Logger logger = LoggerFactory.getLogger(ProductService.class);

    @Autowired
    public ProductService(ProductRepository repo){
        this.productRepo = repo;
    }

    public ResponseEntity<List<ProductResponseDTO>> findAll() {
        var productUntity = productRepo.findAll();
        List<ProductResponseDTO> productsResponseDTO = new ArrayList<ProductResponseDTO>();

        if(productUntity.isEmpty()){
            logger.info("[MS-INFOCAD ] - ::GET ALL PRODUCTS:: - No Content - response: " );
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }else {
        	productUntity.forEach(product -> {
        		productsResponseDTO.add(product.toResponseDTO());
            });
        }
        var response = ResponseEntity.ok().body(productsResponseDTO);

        logger.info("[MS-INFOCAD ] - ::GET ALL PRODUCTS:: - Successful - response: " + response);
        return ResponseEntity.ok().body(productsResponseDTO);
    }

    public ResponseEntity<ProductResponseDTO> findById(Long id) {
        var productEntity = productRepo.findById(id);

        if (productEntity.isPresent()) {
            var response = productEntity.get().toResponseDTO();

            logger.info("[MS-INFOCAD ] - ::GET PRODUCT BY ID:: - Successful - id: " + id + "response: " + response);
            return ResponseEntity.ok().body(response);
        }else{
            logger.info("[MS-INFOCAD ] - ::GET PRODUCT BY ID:: - Not found - response: ");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
    
    public ResponseEntity<ProductResponseDTO> findBySku(Long sku){
        var productEntity = productRepo.findBySku(sku);

        if (productEntity.isPresent()) {
            var response = productEntity.get().toResponseDTO();

            logger.info("[MS-INFOCAD ] - ::GET PRODUCT BY NAME:: - Successful - sku: " + sku + ", response: " + response);
            return ResponseEntity.ok().body(response);
        }else{
            logger.info("[MS-INFOCAD ] - ::GET PRODUCT BY NAME:: - Not found - response: ");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
    
    public ResponseEntity<ProductResponseDTO> findByName(String name){
        var productEntity = productRepo.findByName(name);

        if (productEntity.isPresent()) {
            var response = productEntity.get().toResponseDTO();

            logger.info("[MS-INFOCAD ] - ::GET PRODUCT BY NAME:: - Successful - NAME: " + name + ", response: " + response);
            return ResponseEntity.ok().body(response);
        }else{
            logger.info("[MS-INFOCAD ] - ::GET PRODUCT BY NAME:: - Not found - response: ");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }

    public ResponseEntity<ProductResponseDTO> save(ProductRequestDTO productRequestDTO, UriComponentsBuilder uriBuilder) {

    	ProductEntity productEntity = productRequestDTO.toEntity();
        try {
            var productEntitySaved = productRepo.save(productEntity);
            URI uri = uriBuilder.path("api/v1/product/{id}").buildAndExpand(productEntity.getId()).toUri();
            var response = productEntitySaved.toResponseDTO();

            logger.info("[MS-INFOCAD ] - ::ADD NEW PRODUCT:: - Successful - response: " + response + " uri: " + uri);
            return ResponseEntity.created(uri).body(response);
        }catch (Exception e){
            logger.error("[MS-INFOCAD ] - ::ADD NEW PRODUCT:: - Fail - exception: " + e.getLocalizedMessage());
        }

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }

    public ResponseEntity<?> deleteById(Long id) {
        var productEntity = productRepo.findById(id);

        if(productEntity.isPresent()){
        	productRepo.deleteById(id);
            logger.info("[MS-INFOCAD ] - ::DELETE PRODUCT:: - Successful - id: " + id);
            return ResponseEntity.ok().build();
        }

        logger.info("[MS-INFOCAD ] - ::DELETE PRODUCT:: - No Found - id: " + id);

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();

    }

    public ResponseEntity<?> updateById(Long id, ProductRequestDTO updatedProductRequestDTO) {
        var productEntity = productRepo.findById(id);

        if(productEntity.isPresent()){
        	productEntity.get().update(updatedProductRequestDTO.toEntity());

            logger.info("[MS-INFOCAD ] - ::UPDATE PRODUCT:: - Successful - id: " + id);
            return ResponseEntity.ok().build();
        }

        logger.info("[MS-INFOCAD ] - ::UPDATE PRODUCT:: - No Found - id: " + id);

        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();

    }

}
