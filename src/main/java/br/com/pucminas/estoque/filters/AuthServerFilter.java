package br.com.pucminas.estoque.filters;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.pucminas.estoque.integrations.authserver.impl.AuthIntegrationImpl;
import lombok.NoArgsConstructor;

@Component
@NoArgsConstructor
public class AuthServerFilter implements Filter {

    private AuthIntegrationImpl authServerClient;
    private final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    AuthServerFilter(AuthIntegrationImpl authServerClient){
        this.authServerClient = authServerClient;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        
        
        if(request.getRequestURI().trim().toLowerCase().matches(".*swagger-.*|.*api-docs.*"))
        { 
        	request.getRequestDispatcher(request.getServletPath()).forward(servletRequest, servletResponse);
        	return;
        }
        
        String token = request.getHeader("authorization");

        System.out.println(token);

        if(null != token){
            boolean isAuthorized = this.authServerClient.verifyIfApplicationIsAuthorized(token);

            if(isAuthorized){
                filterChain.doFilter(request, response);
            }else {
                Map<String, Object> errorDetails = new HashMap<>();
                errorDetails.put("messageError", "Invalid token");

                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                response.setContentType(MediaType.APPLICATION_JSON_VALUE);

                mapper.writeValue(response.getWriter(), errorDetails);
            }


        }else {
            Map<String, Object> errorDetails = new HashMap<>();
            errorDetails.put("messageError", "Missing token");

            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);

            mapper.writeValue(response.getWriter(), errorDetails);
        }
    }

    @Override
    public void destroy() {

    }
}
