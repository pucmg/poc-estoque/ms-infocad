package br.com.pucminas.estoque.model.request;

import br.com.pucminas.estoque.model.entities.ProductEntity;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProductRequestDTO {

	private Long sku;
	private String name;
    private String description;
    private float weight;

    public ProductEntity toEntity(){
        return new ProductEntity(this.sku, this.name, this.description, this.weight);
    }
}
