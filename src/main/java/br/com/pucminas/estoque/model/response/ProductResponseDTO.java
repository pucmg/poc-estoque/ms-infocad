package br.com.pucminas.estoque.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProductResponseDTO {
	private Long id;
	private Long sku;
    private String name;
    private String description;
    private float weight;
}