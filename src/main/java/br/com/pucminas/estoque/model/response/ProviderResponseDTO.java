package br.com.pucminas.estoque.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProviderResponseDTO {
	private Long id;
	private String legalName;
    private String publicName;
    private String document;
    private String mainEmail;
    private String specialization;
    private String location;
}
