package br.com.pucminas.estoque.model.request;

import br.com.pucminas.estoque.model.entities.ProviderEntity;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProviderRequestDTO {

    private String legalName;
    private String publicName;
    private String document;
    private String mainEmail;
    private String specialization;
    private String location;

    public ProviderEntity toEntity(){
        return new ProviderEntity(this.legalName, this.publicName, this.document, this.mainEmail, this.specialization, this.location);
    }
}
