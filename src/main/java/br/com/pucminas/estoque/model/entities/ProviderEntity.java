package br.com.pucminas.estoque.model.entities;

import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.Length;

import br.com.pucminas.estoque.model.request.ProviderRequestDTO;
import br.com.pucminas.estoque.model.response.ProviderResponseDTO;
import lombok.*;



@Entity
@Data
@RequiredArgsConstructor
@NoArgsConstructor
@Table(name = "Provider")
public class ProviderEntity {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull @NotEmpty @Length(max = 250) @NonNull
	private String legalName;
	
	@NotNull @NotEmpty @Length(max = 250) @NonNull
	private String publicName;
	
	@NotNull @NotEmpty @Length(max = 20) @NonNull
	private String document;
	
	@NotNull @NotEmpty @Email @NonNull
	private String mainEmail;

	@NotNull @NotEmpty @NonNull
	private String specialization;

	@NotNull @NotEmpty @NonNull
	private String location;
	
	public void update(ProviderEntity newEntity) {
		this.legalName = newEntity.getLegalName();
		this.publicName = newEntity.getPublicName();
		this.document = newEntity.getDocument();
		this.mainEmail = newEntity.getMainEmail();
		this.specialization = newEntity.getSpecialization();
		this.location = newEntity.getLocation();
	}

	public ProviderRequestDTO toDTO(){
		return new ProviderRequestDTO(this.legalName, this.publicName, this.document, this.mainEmail, this.specialization, this.location);
	}

	public ProviderResponseDTO toResponseDTO(){
		return new ProviderResponseDTO(this.id, this.legalName, this.publicName, this.document, this.mainEmail, this.specialization, this.location);
	}
	
}
