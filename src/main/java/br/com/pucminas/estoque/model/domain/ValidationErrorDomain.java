package br.com.pucminas.estoque.model.domain;

public class ValidationErrorDomain {
	
	private String field;
	private String message;
	
	public ValidationErrorDomain(String field, String message) {
		this.field = field;
		this.message = message;
	}

	public String getField() {
		return field;
	}

	public String getMessage() {
		return message;
	}
	
	

}
