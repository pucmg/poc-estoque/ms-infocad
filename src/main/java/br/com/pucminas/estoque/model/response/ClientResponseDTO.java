package br.com.pucminas.estoque.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ClientResponseDTO {
	private Long id;
	private String name;
    private String document;
    private String email;
    private String address;
}