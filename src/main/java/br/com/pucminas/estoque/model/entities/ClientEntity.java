package br.com.pucminas.estoque.model.entities;

import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.Length;

import br.com.pucminas.estoque.model.request.ClientRequestDTO;
import br.com.pucminas.estoque.model.response.ClientResponseDTO;
import lombok.*;


@Entity
@Data
@RequiredArgsConstructor
@NoArgsConstructor
@Table(name = "Client")
public class ClientEntity {
	
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull @NotEmpty @Length(max = 250) @NonNull
	private String name;
	
	@NotNull @NotEmpty @Length(max = 20) @NonNull
	private String document;
	
	@NotNull @NotEmpty @Email @NonNull
	private String email;

	@NotNull @NotEmpty @Length(max = 250) @NonNull
	private String address;
	
	public void update(ClientEntity newEntity) {
		this.name = newEntity.getName();
		this.document = newEntity.getDocument();
		this.email = newEntity.getEmail();
		this.address = newEntity.getAddress();
	}

	public ClientRequestDTO toDTO(){
		return new ClientRequestDTO(this.name, this.document, this.email, this.address);
	}

	public ClientResponseDTO toResponseDTO(){
		return new ClientResponseDTO(this.id, this.name, this.document, this.email, this.address);
	}
		
}
