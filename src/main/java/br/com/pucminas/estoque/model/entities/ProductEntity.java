package br.com.pucminas.estoque.model.entities;

import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.validator.constraints.Length;

import br.com.pucminas.estoque.model.request.ProductRequestDTO;
import br.com.pucminas.estoque.model.response.ProductResponseDTO;
import lombok.*;


@Entity
@Data
@RequiredArgsConstructor
@Table(name = "Product")
public class ProductEntity {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotNull @Min(value = 1) @Max(value = 1000000)
	private Long sku;
	
	@NotNull @NotEmpty @Length(max = 250)
	private String name;
	
	@NotEmpty @Length(max = 2000)
	private String description;
	
	private float weight;
	
	public void update(ProductEntity newEntity) {
		this.sku = newEntity.getSku();
		this.name = newEntity.getName();
		this.description = newEntity.getDescription();
		this.weight = newEntity.getWeight();
	}

	public ProductRequestDTO toDTO(){
		return new ProductRequestDTO(this.sku, this.name, this.description, this.weight);
	}

	public ProductResponseDTO toResponseDTO(){
		return new ProductResponseDTO(this.id, this.sku, this.name, this.description, this.weight);
	}

	public ProductEntity(Long sku2, String name2, String description2, float weight2) {
		this.sku = sku2;
		this.name = name2;
		this.description = description2;
		this.weight = weight2;
	}

}
