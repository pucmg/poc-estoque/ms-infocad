package br.com.pucminas.estoque.model.request;

import br.com.pucminas.estoque.model.entities.ClientEntity;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ClientRequestDTO {

	private String name;
    private String document;
    private String email;
    private String address;

    public ClientEntity toEntity(){
        return new ClientEntity(this.name, this.document, this.email, this.address);
    }
}
