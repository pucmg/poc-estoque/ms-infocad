package br.com.pucminas.estoque;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties
@EntityScan(basePackages = {"br.com.pucminas.estoque.model"})
public class InfoCadApplication {

	public static void main(String[] args) {
		SpringApplication.run(InfoCadApplication.class, args);
	}
	
}
