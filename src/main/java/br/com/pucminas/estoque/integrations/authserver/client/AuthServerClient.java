package br.com.pucminas.estoque.integrations.authserver.client;


import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

import br.com.pucminas.estoque.config.FeignConfiguration;

@FeignClient(name = "AuthServerIntegration", url = "${integration.auth.server.url}", configuration = FeignConfiguration.class)
public interface AuthServerClient {

    @GetMapping("/auth/validate")
    public ResponseEntity<?> isAuthorized(@RequestHeader("authorization") String token);
}
