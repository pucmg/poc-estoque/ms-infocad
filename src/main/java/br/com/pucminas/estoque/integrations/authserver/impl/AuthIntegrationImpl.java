package br.com.pucminas.estoque.integrations.authserver.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import br.com.pucminas.estoque.integrations.authserver.client.AuthServerClient;

@Service
public class AuthIntegrationImpl {

    private AuthServerClient authServerClient;

    @Autowired
    AuthIntegrationImpl(AuthServerClient authServerClient){
        this.authServerClient = authServerClient;
    }

    public boolean verifyIfApplicationIsAuthorized(String token){
        ResponseEntity<?> response = authServerClient.isAuthorized(token);

        if(HttpStatus.OK.value() == response.getStatusCodeValue()){
            return true;
        }else {
            return false;
        }
    }


}
