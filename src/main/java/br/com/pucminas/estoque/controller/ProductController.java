package br.com.pucminas.estoque.controller;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import br.com.pucminas.estoque.model.request.ProductRequestDTO;
import br.com.pucminas.estoque.model.response.ProductResponseDTO;
import br.com.pucminas.estoque.services.ProductService;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/v1")
@Api(tags = "ProductDocumentation")
public class ProductController {

	private final ProductService productService;

	@Autowired
	public ProductController(ProductService userService){
		this.productService = userService;
	}

	@ApiImplicitParam(name = "authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", example = "Bearer access_token")
    @ApiOperation(value = "Get all products")
	@GetMapping("/products")
	public ResponseEntity<List<ProductResponseDTO>> getAllProducts() {
		return productService.findAll();
	}
	
	@ApiImplicitParam(name = "authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", example = "Bearer access_token")
    @ApiOperation(value = "Get product by id")
	@GetMapping("/product/{id}")
	public ResponseEntity<ProductResponseDTO> getProduct(@PathVariable Long id) {
		return productService.findById(id);
	}

	@ApiImplicitParam(name = "authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", example = "Bearer access_token")
    @ApiOperation(value = "Get product by sku")
	@GetMapping("/product/sku/{sku}")
	public ResponseEntity<ProductResponseDTO> getProductBySku(@RequestParam Long sku) {
		return productService.findBySku(sku);
	}
	
	@ApiImplicitParam(name = "authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", example = "Bearer access_token")
    @ApiOperation(value = "Get product by name")
	@GetMapping("/product/name/{name}")
	public ResponseEntity<ProductResponseDTO> getProductByName(@RequestParam String name) {
		return productService.findByName(name);
	}
	
	@ApiImplicitParam(name = "authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", example = "Bearer access_token")
    @ApiOperation(value = "Add new product")
	@PostMapping("/product")
	public ResponseEntity<ProductResponseDTO> addNewProduct(@RequestBody @Valid ProductRequestDTO productRequestDTO, UriComponentsBuilder uriBuilder) {
		return productService.save(productRequestDTO, uriBuilder);
	}
	
	@ApiImplicitParam(name = "authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", example = "Bearer access_token")
    @ApiOperation(value = "Delete product")
	@DeleteMapping("/product/{id}")
	public ResponseEntity<?> deleteProduct(@PathVariable Long id) {
		return productService.deleteById(id);
	}

	@ApiImplicitParam(name = "authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", example = "Bearer access_token")
    @ApiOperation(value = "Update product data")
	@PutMapping("/product/{id}")
	@Transactional
	public ResponseEntity<?> updateProduct(@PathVariable Long id, @RequestBody @Valid ProductRequestDTO newUserDTO){
		return productService.updateById(id, newUserDTO);
	}

}
