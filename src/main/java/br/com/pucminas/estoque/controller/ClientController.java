package br.com.pucminas.estoque.controller;

import java.util.List;

import javax.transaction.Transactional;
import javax.validation.Valid;

import br.com.pucminas.estoque.model.request.ClientRequestDTO;
import br.com.pucminas.estoque.model.response.ClientResponseDTO;
import br.com.pucminas.estoque.services.ClientService;
import io.swagger.annotations.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/v1")
@Api(tags = "ClientDocumentation")
public class ClientController {

	private final ClientService clientService;

	@Autowired
	public ClientController(ClientService userService){
		this.clientService = userService;
	}

	@ApiImplicitParam(name = "authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", example = "Bearer access_token")
	@ApiOperation(value = "Get all clients")
	@GetMapping("/clients")
	public ResponseEntity<List<ClientResponseDTO>> getAllClients() {
		return clientService.findAll();
	}
	
	@ApiImplicitParam(name = "authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", example = "Bearer access_token")
    @ApiOperation(value = "Get client by id")
	@GetMapping("/client/{id}")
	public ResponseEntity<ClientResponseDTO> getClient(@PathVariable Long id) {
		return clientService.findById(id);
	}

	@ApiImplicitParam(name = "authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", example = "Bearer access_token")
    @ApiOperation(value = "Get client by document")
    @GetMapping("/client/document/{document}")
	public ResponseEntity<ClientResponseDTO> getClientByDocument(@RequestParam String document) {
		return clientService.findByDocument(document);
	}
	
	@ApiImplicitParam(name = "authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", example = "Bearer access_token")
    @ApiOperation(value = "Get client by name")
	@GetMapping("/client/name/{name}")
	public ResponseEntity<ClientResponseDTO> getClientByName(@RequestParam String name) {
		return clientService.findByName(name);
	}
	
	@ApiImplicitParam(name = "authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", example = "Bearer access_token")
    @ApiOperation(value = "Add new client")
	@PostMapping("/client")
	public ResponseEntity<ClientResponseDTO> addNewClient(@RequestBody @Valid ClientRequestDTO clientRequestDTO, UriComponentsBuilder uriBuilder) {
		return clientService.save(clientRequestDTO, uriBuilder);
	}
	
	@ApiImplicitParam(name = "authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", example = "Bearer access_token")
    @ApiOperation(value = "Delete client")
	@DeleteMapping("/client/{id}")
	public ResponseEntity<?> deleteClient(@PathVariable Long id) {
		return clientService.deleteById(id);
	}
	
	@ApiImplicitParam(name = "authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", example = "Bearer access_token")
    @ApiOperation(value = "Update client data")
	@PutMapping("/client/{id}")
	@Transactional
	public ResponseEntity<?> updateClient(@PathVariable Long id, @RequestBody @Valid ClientRequestDTO newUserDTO){
		return clientService.updateById(id, newUserDTO);
	}

}
