package br.com.pucminas.estoque.controller;

import br.com.pucminas.estoque.model.request.ProviderRequestDTO;
import br.com.pucminas.estoque.model.response.ProviderResponseDTO;
import br.com.pucminas.estoque.services.ProviderService;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.transaction.Transactional;
import java.util.List;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/api/v1")
@Api(tags = "ProviderDocumentation")
public class ProviderController {

    private ProviderService providerService;

    @Autowired
    ProviderController(ProviderService providerService){
        this.providerService = providerService;
    }

    @ApiImplicitParam(name = "authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", example = "Bearer access_token")
    @ApiOperation(value = "Get all providers")
    @GetMapping("/providers")
    public ResponseEntity<List<ProviderResponseDTO>> getAllProviders() {
        return this.providerService.findAll();
    }

    @ApiImplicitParam(name = "authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", example = "Bearer access_token")
    @ApiOperation(value = "Get provider by id")
    @GetMapping("/provider/{id}")
    public ResponseEntity<ProviderResponseDTO> getProvider(@PathVariable Long id) {
        return this.providerService.findById(id);
    }
    
    @ApiImplicitParam(name = "authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", example = "Bearer access_token")
    @ApiOperation(value = "Get provider by legalName")
    @GetMapping("/provider/legalName/{legalName}")
    public ResponseEntity<ProviderResponseDTO> getProviderByLegalName(@PathVariable String legalName) {
        return this.providerService.findByLegalName(legalName);
    }
    
    @ApiImplicitParam(name = "authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", example = "Bearer access_token")
    @ApiOperation(value = "Get provider by publicName")
    @GetMapping("/provider/publicName/{publicName}")
    public ResponseEntity<ProviderResponseDTO> getProviderByPublicName(@PathVariable String publicName) {
        return this.providerService.findByPublicName(publicName);
    }
    
    @ApiImplicitParam(name = "authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", example = "Bearer access_token")
    @ApiOperation(value = "Get provider by document")
    @GetMapping("/provider/document/{document}")
    public ResponseEntity<ProviderResponseDTO> getProviderByDocument(@PathVariable String document) {
        return this.providerService.findByDocument(document);
    }
    
    @ApiImplicitParam(name = "authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", example = "Bearer access_token")
    @ApiOperation(value = "Get provider by specialization")
    @GetMapping("/providers/specialization/{specialization}")
    public ResponseEntity<List<ProviderResponseDTO>> getProviderBySpecialization(@PathVariable String specialization) {
        return this.providerService.findBySpecialization(specialization);
    }
    
    @ApiImplicitParam(name = "authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", example = "Bearer access_token")
    @ApiOperation(value = "Get provider by location")
    @GetMapping("/providers/location/{location}")
    public ResponseEntity<List<ProviderResponseDTO>> getProviderByLocation(@PathVariable String location) {
        return this.providerService.findByLocation(location);
    }
    
    @ApiImplicitParam(name = "authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", example = "Bearer access_token")
    @ApiOperation(value = "Get providers by specialization and location")
    @GetMapping("/providers/specializationAndLocation/{specialization}/{location}")
    public ResponseEntity<List<ProviderResponseDTO>> getProviderBySpecializationAndLocation(@PathVariable String specialization, @PathVariable String location) {
        return this.providerService.findBySpecializationAndLocation(specialization, location);
    }

    @ApiImplicitParam(name = "authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", example = "Bearer access_token")
    @ApiOperation(value = "Add new provider")
    @PostMapping("/provider")
    public ResponseEntity<ProviderResponseDTO> addNewProvider(@RequestBody ProviderRequestDTO providerRequestDTO, UriComponentsBuilder uriBuilder) {
        return this.providerService.save(providerRequestDTO, uriBuilder);
    }

    @ApiImplicitParam(name = "authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", example = "Bearer access_token")
    @ApiOperation(value = "Delete provider")
    @DeleteMapping("/provider/{id}")
    public ResponseEntity<?> deleteProvider(@PathVariable Long id) {
        return this.providerService.deleteById(id);
    }

    @ApiImplicitParam(name = "authorization", value = "Access Token", required = true, allowEmptyValue = false, paramType = "header", example = "Bearer access_token")
    @ApiOperation(value = "Update provider data")
    @PutMapping("/provider/{id}")
    @Transactional
    public ResponseEntity<?> updateProvider(@PathVariable Long id, @RequestBody ProviderRequestDTO providerRequestDTO) {
        return this.providerService.updateById(id, providerRequestDTO);
    }
}
