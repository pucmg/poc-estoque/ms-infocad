INSERT INTO Provider (LEGAL_NAME, PUBLIC_NAME, DOCUMENT, MAIN_EMAIL, SPECIALIZATION, LOCATION) VALUES ('Promobile Indústria de Móveis LTDA', 'Promobile', '12.345.678/0001-29', 'vendas@promobile.com.br', 'Móveis Planejados', 'Av. Paisagista José Silva de Azevedo Neto, nº 200, Bloco 7, sala 416 - Barra da Tijuca, Rio de Janeiro - RJ, 22775-056');

INSERT INTO Provider (LEGAL_NAME, PUBLIC_NAME, DOCUMENT, MAIN_EMAIL, SPECIALIZATION, LOCATION) VALUES ('Rennova Indústria de Móveis LTDA', 'Rennova Ambientes Planejados', '23.456.789/0001-29', 'vendas@rennova.com.br', 'Móveis Planejados', 'Estr. dos Três Rios, 336 - Loja - Freguesia de Jacarepaguá, Rio de Janeiro - RJ, 22745-005');

INSERT INTO Provider (LEGAL_NAME, PUBLIC_NAME, DOCUMENT, MAIN_EMAIL, SPECIALIZATION, LOCATION) VALUES ('Castellato Indústria de Móveis LTDA', 'Castellato Móveis Planejados', '12.345.678/0001-29', 'vendas@catellato.com.br', 'Móveis Planejados', 'Av. Gov. Roberto Silveira, 373 - Centro, Nova Iguaçu - RJ, 26210-210');

INSERT INTO Client (NAME, DOCUMENT, EMAIL, ADDRESS) VALUES ('Jurandir Figueira Representações', '10.340.670/0001-20', 'contato@jurandir.com.br', 'Av. 13, 57, Nossa Senhora do Socorro, Sergipe');

INSERT INTO Client (NAME, DOCUMENT, EMAIL, ADDRESS) VALUES ('Amaro Batista Santos', '087.231.234-22', 'amaro@gmail.com', 'Rua projetada xxv, 15, Asa Leste, Brasília');

INSERT INTO Product (SKU, NAME, DESCRIPTION, WEIGHT) VALUES (12345,'COZINHA MODULAR XINAGUA', 'CONJUNTO DE ARTEFATOS DE COZINHA MODULAR, BRANCOS, CONTENDO 1 AEREO, 1 BANCADA, 2 CONSOLES LATERAIS', 40.5);

INSERT INTO Product (SKU, NAME, DESCRIPTION, WEIGHT) VALUES (51343,'SOFÁ MUZIMBA', 'SOFÁ MODULAR, CONTENDO 3 MÓDULOS RETRATEIS, COBERTOS DE CURVIM E CETIM', 152.8);

