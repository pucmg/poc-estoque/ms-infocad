package br.com.pucminas.estoque.fixturefactory.template;

import br.com.pucminas.estoque.model.request.ClientRequestDTO;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;

public class FixtureUserTemplate implements TemplateLoader {
    @Override
    public void load() {
        Fixture.of(ClientRequestDTO.class).addTemplate("valid", new Rule(){
            {
                add("firstName", random("Leonardo", "Lucas", "Mateus", "Marina", "Janaina", "Vitor"));
                add("lastName", random("Lopes", "Souza", "Silva", "Nascimento", "Alves", "Pereira"));
                add("document", random("07863208009","75367773028", "50434699004", "58317397084"));
                add("email", "user@hotmail.com");
            }
        });
    }
}
