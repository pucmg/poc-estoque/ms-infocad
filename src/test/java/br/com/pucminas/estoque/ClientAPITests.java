package br.com.pucminas.estoque;

import br.com.pucminas.estoque.model.request.ClientRequestDTO;
import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.google.gson.Gson;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;


@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ClientAPITests {

	private String host = "http://localhost:8080";

	@BeforeClass
	public static void loadFixtureFactory() {
		FixtureFactoryLoader.loadTemplates("br.com.alelo.api.fixturefactory.template");
	}

	@Test
	public void wasAddedClientWithSuccess() {

		ClientRequestDTO clientDTO = Fixture.from(ClientRequestDTO.class).gimme("valid");

	    given()
				.body(new Gson().toJson(clientDTO))
				.contentType("application/json")
		.when()
			.post("/api/v1/client")
		.then()
			.assertThat()
			.statusCode(201);
	}

	@Test
	public void validateGetClientrById(){

		ClientRequestDTO clientDTO = Fixture.from(ClientRequestDTO.class).gimme("valid");

		String location = given()
				.body(new Gson().toJson(clientDTO))
				.contentType("application/json")
				.when()
				.post("/api/v1/client")
				.then()
				.assertThat()
				.statusCode(201)
				.extract()
				.header("Location");

		given()
				.contentType("application/json")
				.when()
				.get(location.replace(host, ""))
				.then()
				.assertThat()
				.statusCode(200);

	}

	@Test
	public void validateGetAllClients() {
		String response = given()
				.get("/api/v1/clients")
				.then()
				.assertThat()
				.statusCode(200)
				.extract()
				.response()
				.body()
				.asString();

		ClientRequestDTO[] clientsArray = new Gson().fromJson(response, ClientRequestDTO[].class);
		List<ClientRequestDTO> users = Arrays.asList(clientsArray);

		assertThat(users.size(), greaterThan(1));

	}

	@Test
	public void validateDeleteById() {

		ClientRequestDTO clientsDTO = Fixture.from(ClientRequestDTO.class).gimme("valid");

		String location = given()
				.body(new Gson().toJson(clientsDTO))
				.contentType("application/json")
				.when()
				.post("/api/v1/client")
				.then()
				.assertThat()
				.statusCode(201)
				.extract()
				.header("Location");

			given()
				.contentType("application/json")
				.when()
				.delete(location.replace(host, ""))
				.then()
				.assertThat()
				.statusCode(200);

		given()
				.contentType("application/json")
				.when()
				.get(location.replace(host, ""))
				.then()
				.assertThat()
				.statusCode(404);


	}

	@Test
	public void validateUpdateById() {

		ClientRequestDTO client1 = Fixture.from(ClientRequestDTO.class).gimme("valid");
		ClientRequestDTO client2 = Fixture.from(ClientRequestDTO.class).gimme("valid");

		String location = given()
				.body(new Gson().toJson(client1))
				.contentType("application/json")
				.when()
				.post("/api/v1/client")
				.then()
				.assertThat()
				.statusCode(201)
				.extract()
				.header("Location");

		given()
				.body(new Gson().toJson(client2))
				.contentType("application/json")
				.when()
				.put(location.replace(host, ""))
				.then()
				.assertThat()
				.statusCode(200);

		String response = given()
				.contentType("application/json")
				.when()
				.get(location.replace(host, ""))
				.then()
				.assertThat()
				.statusCode(200)
				.extract()
				.response()
				.body()
				.asString();

		ClientRequestDTO clientUpdated = new Gson().fromJson(response, ClientRequestDTO.class);

		assertNotEquals(client1, clientUpdated);


	}

}
